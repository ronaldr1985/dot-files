--	_			 _ _		 _
-- (_)_ __ (_) |_	| |_	 _	__ _
-- | | '_ \| | __| | | | | |/ _` |
-- | | | | | | |_ _| | |_| | (_| |
-- |_|_| |_|_|\__(_)_|\__,_|\__,_|
--
-- Filename: .config/nvim/init.vim
-- Git Repo: https://gitlab.com/ronaldr1985/dot-files
-- Maintainer: Ronald1985

-- Variables {{{

local vim = vim
local api = vim.api

-- }}}

-- Plugins {{{

local Plug = vim.fn['plug#']

-- Specify a directory for plugins
vim.call('plug#begin')
Plug ('fatih/vim-go', {['do'] = ':GoUpdateBinaries'})
Plug 'preservim/nerdcommenter'
Plug 'itchyny/vim-cursorword'
Plug 'jiangmiao/auto-pairs'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'sheerun/vim-polyglot'
Plug 'christoomey/vim-tmux-navigator'
Plug 'ntpeters/vim-better-whitespace'
Plug 'pearofducks/ansible-vim'
Plug 'dense-analysis/ale'
Plug 'nvim-lualine/lualine.nvim'
Plug 'nvim-tree/nvim-web-devicons'
Plug 'shmup/vim-sql-syntax'
Plug 'Shirk/vim-gas'
Plug 'Tetralux/odin.vim'
Plug 'sudar/vim-arduino-syntax'
Plug 'junegunn/goyo.vim'
Plug 'neovim/nvim-lspconfig'
Plug 'jvirtanen/vim-hcl'
Plug 'egberts/vim-syntax-bind-named'
Plug 'preservim/nerdtree'
Plug 'gmist/vim-palette'
Plug 'junegunn/vim-easy-align'
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'onsails/lspkind.nvim'
Plug('L3MON4D3/LuaSnip', {['tag'] = 'v2.*', ['do'] = 'make install_jsregexp'})
Plug 'preservim/vimux'
vim.call('plug#end')

-- }}}

-- LuaLine Settings {{{

require('lualine').setup {
	options = {
		icons_enabled = true,
		theme = 'ayu_dark',
		component_separators = { left = '', right = ''},
		section_separators = { left = '', right = ''},
		disabled_filetypes = {
			statusline = {},
			winbar = {},
		},
		ignore_focus = {},
		always_divide_middle = true,
		globalstatus = false,
		refresh = {
			statusline = 1000,
			tabline = 1000,
			winbar = 1000,
		}
	},
	sections = {
		lualine_a = {'mode'},
		lualine_b = {'branch', 'diff', 'diagnostics'},
		lualine_c = {'filename'},
		lualine_x = {'encoding', 'fileformat', 'filetype'},
		lualine_y = {'progress'},
		lualine_z = {'location'}
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {'filename'},
		lualine_x = {'location'},
		lualine_y = {},
		lualine_z = {}
	},
	tabline = {},
	winbar = {},
	inactive_winbar = {},
	extensions = {}
}

-- }}}

-- NerdCommenter Settings {{{

-- Add spaces after comment delimiters by default
api.nvim_set_var("NERDSpaceDelims", 1)

-- Use compact syntax for prettified multi-line comments
api.nvim_set_var("NERDCompactSexyComs ", 1)

-- Disable the default NERDCommenter keybinds
api.nvim_set_var("NERDCreateDefaultMappings ", 0)

-- }}}

-- Random {{{

api.nvim_command [[set encoding=utf-8]]

-- maintain undo history between sessions
vim.cmd([[
set undofile
]])

-- }}}

-- Colours {{{

api.nvim_command [[syntax on]]
api.nvim_command [[set background=dark]]
api.nvim_command [[set termguicolors]]
api.nvim_command [[colorscheme freshcut-contrast]]

-- }}}

-- UI {{{

-- Set relative number
vim.wo.relativenumber = true

-- Still show the actual line number though
vim.wo.number = true

-- Colour column
vim.wo.colorcolumn = "120"

-- i sometimes use the mouse to scroll through a buffer
vim.cmd [[set mouse=a]]

-- Show tabs
-- api.nvim_command [[:set list lcs=tab:\.\ ]]

api.nvim_command [[
set list
set listchars=tab:▸·

highlight NonText guifg=#4a4a59
highlight SpecialKey guifg=#4a4a59
]]

-- Splits open at the bottom and the right
api.nvim_command [[set splitbelow splitright]]

-- }}}

-- Keybindings {{{

vim.g.mapleader = ","

-- Save with Control-S
vim.cmd([[
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>
]])

-- NERDCommenter
api.nvim_set_keymap("n", "<C-/>", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("v", "<C-/>", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("n", "", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("v", "", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("n", "<leader>/", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("v", "<leader>/", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})

-- Enable spell check
api.nvim_set_keymap("n", "<leader>o", ":setlocal spell spelllang=en_gb<CR>", {noremap=true, silent=false})

-- Map <leader>v to vsplit
api.nvim_set_keymap("n", "<leader>v", ":vsplit<CR>", {noremap=true})

-- Control [hjkl] moves between splits
api.nvim_set_keymap("n", "<C-h>", "<C-W><C-h>", {noremap=true})
api.nvim_set_keymap("n", "<C-j>", "<C-W><C-j>", {noremap=true})
api.nvim_set_keymap("n", "<C-k>", "<C-W><C-k>", {noremap=true})
api.nvim_set_keymap("n", "<C-l>", "<C-W><C-l>", {noremap=true})

-- Clear vim search
api.nvim_set_keymap("n", "<leader>z", ":noh<CR>", {noremap=true})

-- Toggle vim ALE
api.nvim_set_keymap("n", "<leader>a", ":ALEToggle<CR>", {noremap=true})

-- Next ALE Error
api.nvim_set_keymap("n", "<leader>an", ":ALENext<CR>", {noremap=true})
--
-- Previous ALE Error
api.nvim_set_keymap("n", "<leader>ap", ":ALEPrevious<CR>", {noremap=true})


-- Goyo plugin makes text more readable when writing prose:
api.nvim_set_keymap("n", "<leader>a", ":Goyo<CR>", {noremap=true})

-- Toggle autopairs
api.nvim_command [[let g:AutoPairsShortcutToggle = '<F3>']]

-- Close current buffer
api.nvim_set_keymap("n", "<leader>q", ":bp<BAR>bd#<CR>", {noremap=true, silent=true})

-- Create vertical split from current buffer
api.nvim_set_keymap("n", "<leader>t", ":bp<BAR>bd#<CR>", {noremap=true, silent=true})

-- Go to tab by number
api.nvim_set_keymap("n", "<leader>1", "1gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>2", "2gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>3", "3gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>4", "4gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>5", "5gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>6", "6gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>7", "7gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>8", "8gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>9", "9gt", {noremap=true})

-- Replace tabs with spaces
api.nvim_set_keymap("n", "<leader>`", ":set expandtab<CR>", {noremap=true})

-- Focus NERDTree
api.nvim_set_keymap("n", "<leader>n", ":NERDTreeFocus<CR>", {noremap=true, silent=true})

-- Toggle NERDTree
api.nvim_set_keymap("n", "<C-n>", ":NERDTreeToggle<CR>", {noremap=true, silent=true})

-- Toggle NERDTreeFind
api.nvim_set_keymap("n", "<leader>n", ":NERDTreeFind<CR>", {noremap=true, silent=true})

-- Start interactive EasyAlign in visual mode (e.g. vipga)
vim.cmd [[xmap ea <Plug>(EasyAlign)]]

-- Start interactive EasyAlign for a motion/text object (e.g. gaip)
vim.cmd [[nmap ea <Plug>(EasyAlign)]]

-- Go to next tab
api.nvim_set_keymap("n", "<C-tab>", ":tabNext<CR>", {noremap=true, silent=true})
api.nvim_set_keymap("n", "<C-PageDown>", ":tabNext<CR>", {noremap=true, silent=true})

-- Go to previous tab
api.nvim_set_keymap("n", "<C-S-tab>", ":tabNext<CR>", {noremap=true, silent=true})
api.nvim_set_keymap("n", "<C-PageUp>", ":tabNext<CR>", {noremap=true, silent=true})

vim.api.nvim_set_keymap('i', '<C-BS>', '<C-W>', {noremap = true})

-- }}}

-- File stuff {{{

vim.cmd [[autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff]]
vim.cmd [[autocmd BufRead,BufNewFile *.tex set filetype=tex]]
vim.cmd [[autocmd BufRead,BufNewFile *.h,*.c set filetype=c]]
vim.cmd [[autocmd BufRead *.sql set filetype=mysql]]
vim.cmd [[au BufRead,BufNewFile *.yml,*.yaml set filetype=yaml.ansible]]
vim.cmd [[autocmd BufEnter *.yml,*.yaml ALEDisable]] -- Turn off ALE for yaml files
vim.cmd [[autocmd BufRead,BufNewFile Jenkinsfile set filetype=groovy]]
vim.cmd [[autocmd BufRead *.odin set filetype=odin]]

vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.shiftwidth = 4
vim.o.smarttab = true

-- Case insensitive searches
vim.o.ignorecase = true

-- but still want search to be smart. If i type a upper case thing, do a case
-- sensitive blah
vim.o.smartcase = true

-- }}}

-- Folding {{{

vim.cmd [[set foldenable]]
vim.cmd[[set foldlevelstart=10]]
vim.cmd[[set foldnestmax=10]]
vim.cmd[[set foldmethod=syntax]]
 -- Open and close all folds
vim.cmd[[nnoremap <space> za]]
vim.cmd[[nnoremap <space> za]]

-- }}}

-- Language servers {{{

local lspconfig = require('lspconfig')
local lsp = require('lspconfig')

-- Go Language Server
lspconfig.gopls.setup({
	settings = {
		gopls = {
			analyses = {
				unusedparams = true,
			},
			staticcheck = true,
			gofumpt = true,
		},
	},
})

-- Odin Language server
lspconfig.ols.setup({})

lspconfig.ccls.setup {
  init_options = {
    cache = {
      directory = ".ccls-cache";
    };
  }
}

lspconfig.pyright.setup{}

-- }}}

-- init.vim - Sourcing and editing {{{

api.nvim_set_keymap("n", "<leader>ev", ":e ~/.config/nvim/init.lua<CR>", {noremap=true})
api.nvim_set_keymap("n", "<leader>sv", ":source ~/.config/nvim/init.lua <bar> :doautocmd BufRead<CR>", {noremap=true})

-- }}}

-- Ansible {{{

-- Indentation will completely reset (unindent to column 0)
vim.cmd[[let g:ansible_unindent_after_newline = 1]]

-- Dim the instances of name: found
vim.cmd[[let g:ansible_name_highlight = 'b']]

-- }}}

-- Terminal stuff {{{

vim.cmd([[
" Terminal Function
let g:term_buf = 0
let g:term_win = 0
function! TermToggle(height)
		if win_gotoid(g:term_win)
				hide
		else
				botright new
				exec "resize " . a:height
				try
						exec "buffer " . g:term_buf
				catch
						call termopen($SHELL, {"detach": 0})
						let g:term_buf = bufnr("")
						set nonumber
						set norelativenumber
						set signcolumn=no
				endtry
				startinsert!
				let g:term_win = win_getid()
		endif
endfunction
]])

vim.cmd([[
" Toggle terminal on/off (neovim)
nnoremap <C-'> :call TermToggle(16)<CR>
inoremap <C-'> <Esc>:call TermToggle(16)<CR>
tnoremap <C-'> <C-\><C-n>:call TermToggle(16)<CR>
nnoremap <A-'> :call TermToggle(16)<CR>
inoremap <A-'> <Esc>:call TermToggle(16)<CR>
tnoremap <A-'> <C-\><C-n>:call TermToggle(16)<CR>
nnoremap <A-`> :call TermToggle(16)<CR>
inoremap <A-`> <Esc>:call TermToggle(16)<CR>
tnoremap <A-`> <C-\><C-n>:call TermToggle(16)<CR>

" Terminal go back to normal mode
tnoremap <Esc> <C-\><C-n>
tnoremap :q! <C-\><C-n>:q!<CR>
]])

-- }}}

-- ALE {{{

-- Toggle autopairs
api.nvim_command [[let g:ale_python_auto_virtualenv=1]]

api.nvim_command[[let g:ale_linters = { 'c': ['clang']}]]

-- Use C89 standard
-- api.nvim_command [[let g:ale_c_clang_options = '-Wall -std=c89']]

-- }}}

-- NERDTree {{{

-- Start NERDTree and put the cursor back in the other window.
-- vim.cmd([[
--	 autocmd VimEnter * NERDTree | wincmd p
-- ]])

vim.cmd([[
	" Exit Vim if NERDTree is the only window remaining in the only tab.
	autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | call feedkeys(":quit\<CR>:\<BS>") | endif
]])

-- }}}

-- vim-better-whitespace {{{

-- Start NERDTree and put the cursor back in the other window.
vim.cmd([[
	let g:better_whitespace_enabled=1
	let g:strip_whitespace_on_save=1
]])

-- }}}

-- Telescope {{{

local telescope_builtin = require('telescope.builtin')

require('telescope').setup({
  pickers = {
    buffers = {
      show_all_buffers = true,
      sort_mru = true,
      mappings = {
        i = {
          ["<C-BS>"] = "delete_buffer",
          ["<C-H>"] = "delete_buffer",
        },
      },
    },
  },
})

-- Search current directory
vim.keymap.set('n', '<F4>', telescope_builtin.find_files, {})
vim.keymap.set('n', '<C-f>', telescope_builtin.find_files, {})

-- Search buffers
vim.keymap.set('n', '<leader>b', telescope_builtin.buffers, {})

-- Search help
vim.keymap.set('n', '<leader>fh', telescope_builtin.help_tags, {})

-- Show diagnostics
vim.keymap.set('n', '<leader>n', telescope_builtin.diagnostics, {})

-- Search in all files in directory
vim.keymap.set('n', '<leader>f', telescope_builtin.live_grep, {})

-- }}}

-- Autocomplete {{{

vim.opt.completeopt = { "menu", "menuone", "noselect" }
vim.opt.shortmess:append "c"

-- lspkind {{{

local lspkind = require "lspkind"
lspkind.init{}

-- }}}

-- LuaSnip {{{

local ls = require "luasnip"
ls.config.set_config {
	history = false,
	updateevents = "TextChanged,TextChangedI"
}

for _, ft_path in ipairs(vim.api.nvim_get_runtime_file("~/.config/nvim/snippets/*.lua", true)) do
	loadfile(ft_path)()
end

-- }}}

-- nvim-cmp {{{

-- Set up nvim-cmp.
local cmp = require'cmp'

cmp.setup {
	sources = {
		{ name = "nvim_lsp" },
		{ name = "path" },
		{ name = "buffer" },
	},
	mapping = {
		["<Down>"] = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert },
		["<Up>"]   = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Insert },
		["<C-n>"]  = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert },
		["<C-p>"]  = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Insert },
		["<Tab>"]   = cmp.mapping(
			cmp.mapping.confirm {
				behavior = cmp.ConfirmBehavior.Insert,
				select = true,
			}
		),
		["<CR>"]   = cmp.mapping(
			cmp.mapping.confirm {
				behavior = cmp.ConfirmBehavior.Insert,
				select = true,
			}
		),
	},
}

-- }}}

-- }}}

-- vim:foldmethod=marker:foldlevel=0


