#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Please pass one string to the program\n");
		exit(1);
	}
	printf("%s: %lu\n", argv[1], strlen(argv[1]));
}


