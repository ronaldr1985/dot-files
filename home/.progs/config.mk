CC = clang

# Folder containing all the progs
SRC_DIR=./src/*

# Path
BIN_FOLDER = /home/$$USER/.local/bin

INCS =
LIBS =

CFLAGS  = -g -Wall -std=c99 -pedantic $(INCS) $(LIBS)

