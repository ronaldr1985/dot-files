export PATH="$HOME/.local/bin:$(du $HOME/.scripts/ | cut -f2 | tr '\n' ':')$HOME/.go/bin/:$PATH"

## nnn
export NNN_BMS='d:~/dox;i:~/images;D:~/dl/;p:~/pix;r:~/repos;v:~/vids;m:~/music'

## go
export GOPATH=$HOME/.go

## organizemsuic
export MUSICDIR=~/music

## ls
LS_COLORS=$LS_COLORS:'di=0;32:' ; export LS_COLORS

## Default Applications
export EDITOR=vim
export TERM=alacritty
export BROWSER=firefox
export CC=clang

## Arduino IDE
export _JAVA_AWT_WM_NONREPARENTING=1
export AWT_TOOLKIT=MToolkit
