# My personal dotfiles

## Installation

Clone this repository into your home directory. Then run the stowrestore script, which will create symbolic links of the dot files. The script will also install Vim Plug, the Vim PlugIn manager, found [here](https://github.com/junegunn/vim-plug)
